#!/usr/bin/python3

import sys

sys.path.insert(0, './leet_code')
import two_cities
import reverse_string

def leet_code(challenge):
    if challenge == 'two_cities':
        cost = two_cities.schedule_cost([[10,20],[30,200],[400,50],[30,20]])
        print('Expect 110:', cost)
    elif challenge == 'reverse_string':
        s = 'hello'
        s_rev = 'hello'
        #reverse_string.reverse_string(s_rev)
        print('Reversing "', s, '":', s_rev)
    else:
        print(challenge, 'is not a valid challenge for Leet Code.')


def main(argv):
    if len(argv) < 3:
        print('Please pass the website and the specific challenge as arguments to main.py')
        return

    website = argv[1]
    challenge = argv[2]

    if website == 'leet_code':
        leet_code(challenge)
        return
    else:
        print(website, 'is not a valid website option.')


if __name__ == '__main__':
    main(sys.argv)
