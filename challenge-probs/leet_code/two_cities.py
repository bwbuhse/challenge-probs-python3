from typing import List


def schedule_cost(costs: List[List[int]]) -> int:
    N = len(costs)//2
    refunds = []
    total_cost = 0
    for to_a, to_b in costs:
        total_cost += to_a
        refunds.append(to_b - to_a)

    refunds.sort()
    for i in range(N):
        # Refund the N biggest refunds (since it's cheaper for them to go to B instead)
        total_cost += refunds[i]

    return total_cost
