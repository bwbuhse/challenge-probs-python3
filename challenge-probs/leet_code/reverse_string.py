from typing import List


def reverse_string(s: List[str]) -> None:
    for i in range(len(s)):
        j = len(s) - 1 - i
        if i >= j:
            return
        else:
            temp = s[i]
            s[i] = s[j]
            s[j] = temp

